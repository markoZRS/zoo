import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MammalsComponent } from './mammals/mammals.component';
import { BirdsComponent } from './birds/birds.component';

@NgModule({
  declarations: [
    AppComponent,
    BirdsComponent
    MammalsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
