import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mammals',
  template: `
      <p style="text-align: center; font-size: 30px;">Mammals</p>
      <hr>
      <div *ngFor="let mammal of mammals">
        <img [src]="mammal" style="display: block; margin-left: auto; margin-right: auto; width: 60%;">
        <br>
      </div>
      <hr>`,
  styleUrls: ['./mammals.component.css']
})
export class MammalsComponent implements OnInit {
  mammals = [
    // tslint:disable-next-line:max-line-length
    'https://images2.minutemediacdn.com/image/upload/c_crop,h_2832,w_4208,x_24,y_0/v1560272025/shape/mentalfloss/19116-istock-157478834.jpg?itok=MlgnbmsO',
    'https://www.nationalgeographic.com/content/dam/animals/pictures/hero/mammals-hero.ngsversion.1498492837757.adapt.1900.1.jpg',
    // tslint:disable-next-line:max-line-length
    'https://images.theconversation.com/files/72513/original/image-20150219-28204-1iw4k6l.jpg?ixlib=rb-1.1.0&q=45&auto=format&w=926&fit=clip'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
