import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-birds',
  template: `
      <p style="text-align: center; font-size: 30px;">Birds</p>
      <hr>
      <div *ngFor="let bird of birds">
        <img [src]="bird" style="display: block; margin-left: auto; margin-right: auto; width: 60%;">
        <br>
      </div>
      <hr>`,
  styleUrls: ['./birds.component.css']
})
export class BirdsComponent implements OnInit {

  birds = [
    'https://ichef.bbci.co.uk/news/1024/branded_news/67CF/production/_108857562_mediaitem108857561.jpg',
    'https://scx2.b-cdn.net/gfx/news/hires/2019/uscresearche.jpg'
  ];

  constructor() { }

  ngOnInit(): void {
  }

}
